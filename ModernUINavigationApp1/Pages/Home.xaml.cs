﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Threading;
using System.IO;
using FirstFloor.ModernUI.Windows.Controls;

namespace ModernUINavigationApp1.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();       
        }
        private void EnabledControlsRadioButtons()
        {
            textboxmaska.IsEnabled = radiobtnmaska.IsChecked.Value;
            textboxcustrash.IsEnabled = radiobuttoncustomrash.IsChecked.Value;
            comboBoxrash.IsEnabled = radiobuttonrash.IsChecked.Value;
        }

        string[] file_list;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                string put = string.Empty;
                 
                if (checkboxrabstol.IsChecked.Value)
                {
                    put = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                }
                else
                {
                    
                    if (textboxput.Text.Trim() == "")
                    {
                        ModernDialog.ShowMessage("Введите путь", "Ошибка", MessageBoxButton.OK);
                        return;
                    }
                    else
                    {
                        put = textboxput.Text;
                    }

                }
                
                if (radiobuttonrash.IsChecked.Value)
                {
                    file_list = Directory.GetFiles(put, "*" + comboBoxrash.Text);

                }
                else if (radiobuttoncustomrash.IsChecked.Value)
                {
                    if (!textboxcustrash.Text.Contains("."))
                    {
                        ModernDialog.ShowMessage("Расширение указывается с точкой", "Ошибка", MessageBoxButton.OK);
                        return;
                        
                    }
                    file_list = Directory.GetFiles(put, "*" + textboxcustrash.Text);
                }
                else if (radiobtnmaska.IsChecked.Value)
                {
                    if(textboxmaska.Text.Trim() == "")
                    {
                        ModernDialog.ShowMessage("Введите маску", "Ошибка", MessageBoxButton.OK);
                        return;
                    }
                    file_list = Directory.GetFiles(put, textboxmaska.Text);
                }

                Thread t = new Thread(work);
                t.Start();
                t.Join();
                
            }
            catch
            {
                ModernDialog.ShowMessage("Произошла ошибка", "Ошибка", MessageBoxButton.OK);
            }
        }

        
        
       private void work ()
        {
            
            if (Work.Delete(file_list))
            {
                Dispatcher.BeginInvoke(new Action(delegate
                {
                    ModernDialog.ShowMessage("Удалено: " + file_list.Length + " файлов", "Успешно", MessageBoxButton.OK);
                }));
                
            }
            else
            {
                Dispatcher.BeginInvoke(new Action(delegate
                {
                    ModernDialog.ShowMessage("Произошла ошибка", "Ошибка", MessageBoxButton.OK);
                }));
                
            }
        }
        
              

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            EnabledControlsRadioButtons();
        }

        private void radiobtnmaska_Click(object sender, RoutedEventArgs e)
        {
            EnabledControlsRadioButtons();
        }

        private void radiobuttonrash_Click(object sender, RoutedEventArgs e)
        {
            EnabledControlsRadioButtons();
        }

        private void radiobuttoncustomrash_Click(object sender, RoutedEventArgs e)
        {
            EnabledControlsRadioButtons();
        }

        private void checkboxrabstol_Click(object sender, RoutedEventArgs e)
        {
            if(checkboxrabstol.IsChecked.Value)
            {
                textboxput.Text = "";
            }
            textboxput.IsEnabled = !checkboxrabstol.IsChecked.Value;
        }
    }
}
